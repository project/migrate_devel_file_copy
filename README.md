CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * Maintainers


INTRODUCTION
------------

This module contains a single migrate process plugin `file_copy_or_generate`.
As the plugin's ID suggests, it is able to generate files: if the specified
source file is missing, the plugin tries to generate a file on the destination
site.


REQUIREMENTS
------------

No hard dependencies, but you have to enable the Migrate module from Drupal core
in order being able to use this module as well :).


INSTALLATION
------------

You can install Migrate Devel FileCopy as you would normally install a
contributed Drupal 8 or 9 module.


CONFIGURATION
-------------

This module does not have any configuration option.


USAGE
-----

TODO.


MAINTAINERS
-----------

* Zoltán Horváth (huzooka) - https://www.drupal.org/u/huzooka

This project has been sponsored by [Acquia][6].

[1]: https://acquia.com
