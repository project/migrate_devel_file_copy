<?php

declare(strict_types=1);

// cspell:ignore imagebmp
namespace Drupal\Tests\migrate_devel_file_copy\Kernel\Plugin\migrate\process;

use Drupal\Tests\migrate\Kernel\process\FileCopyTest;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrateProcessInterface;
use Drupal\migrate\Row;
use Drupal\migrate_devel_file_copy\Plugin\migrate\process\FileCopyOrGenerate;

/**
 * Tests the file_copy_or_generate process plugin.
 *
 * @group migrate_devel_file_copy
 *
 * @coversDefaultClass \Drupal\migrate_devel_file_copy\Plugin\migrate\process\FileCopyOrGenerate
 */
class FileCopyOrGenerateTest extends FileCopyTest {

  /**
   * {@inheritdoc}
   */
  public function testDownloadRemoteUri(): void {
    $executable = $this->prophesize(MigrateExecutableInterface::class)->reveal();
    $row = $this->prophesize(Row::class)->reveal();
    $download_plugin = $this->createMock(MigrateProcessInterface::class);
    $download_plugin
      ->expects($this->once())
      ->method('transform');

    $plugin = new FileCopyOrGenerate(
      [],
      'file_copy_or_generate',
      [],
      $this->container->get('stream_wrapper_manager'),
      $this->container->get('file_system'),
      $download_plugin
    );

    $plugin->transform(
      [
        'https://drupal.org/favicon.ico',
        '/destination/path',
      ],
      $executable,
      $row,
      'destination_property'
    );
  }

  /**
   * Tests that missing files are generated.
   *
   * @param string|null $source
   *   The source of the missing file.
   * @param string|null $destination
   *   The destination path of the missing file.
   * @param string|null $expected_mime
   *   The expected mime type of the generated file.
   *
   * @dataProvider providerTestNonExistentSourceFile
   */
  public function testNonExistentSourceFile(string $source = '', string $destination = '', string $expected_mime = ''): void {
    $actual_destination = $this->doTransform($source, $destination);
    $this->assertFileExists($actual_destination);
    $this->assertSame($actual_destination, $destination, "The process plugin wasn't able to generate the file");
    if (extension_loaded('fileinfo')) {
      $actual_mime_type = finfo_file(
        finfo_open(FILEINFO_MIME_TYPE),
        $actual_destination
      );
      $this->assertEquals($expected_mime, $actual_mime_type);
    }
  }

  /**
   * Data provider for ::testNonExistentSourceFile.
   *
   * @return array[]
   *   The test cases.
   */
  public static function providerTestNonExistentSourceFile(): array {
    $bmpMime = 'text/plain';
    if (function_exists('imagebmp')) {
      $bmpMime = version_compare(PHP_VERSION, '8.3', 'ge')
        ? 'image/bmp'
        : 'image/x-ms-bmp';
    }
    return [
      'jpg' => [
        'Source' => '/missing/image.jpg',
        'Destination' => 'public://existing/image.jpg',
        'Expected mime type' => 'image/jpeg',
      ],
      'jpeg' => [
        'Source' => '/missing/image.jpeg',
        'Destination' => 'public://existing/image.jpeg',
        'Expected mime type' => 'image/jpeg',
      ],
      'JPG' => [
        'Source' => '/missing/iMaGe.JPG',
        'Destination' => 'public://existing/iMaGe.JPG',
        'Expected mime type' => 'image/jpeg',
      ],
      'gif' => [
        'Source' => '/missing/image.gif',
        'Destination' => 'public://existing/image.gif',
        'Expected mime type' => 'image/gif',
      ],
      'png' => [
        'Source' => '/missing/image.png',
        'Destination' => 'public://existing/image.png',
        'Expected mime type' => 'image/png',
      ],
      'bmp' => [
        'Source' => '/missing/image.bmp',
        'Destination' => 'public://existing/image.bmp',
        'Expected mime type' => $bmpMime,
      ],
      'webp' => [
        'Source' => '/missing/image.webp',
        'Destination' => 'public://existing/image.webp',
        'Expected mime type' => 'text/plain',
      ],
      'txt' => [
        'Source' => '/missing/file.txt',
        'Destination' => 'public://existing/file.txt',
        'Expected mime type' => 'text/plain',
      ],
      'pdf' => [
        'Source' => '/missing/file.pdf',
        'Destination' => 'public://existing/file.pdf',
        'Expected mime type' => 'text/plain',
      ],
      'zip' => [
        'Source' => '/missing/file.zip',
        'Destination' => 'public://existing/file.zip',
        'Expected mime type' => 'text/plain',
      ],
      'gz' => [
        'Source' => '/missing/file.gz',
        'Destination' => 'public://existing/file.gz',
        'Expected mime type' => 'text/plain',
      ],
      'no extension' => [
        'Source' => '/missing/file',
        'Destination' => 'public://existing/file',
        'Expected mime type' => 'text/plain',
      ],
      'Missing remote txt' => [
        'Source' => 'http://localhost/missing/file.txt',
        'Destination' => 'public://existing/file.txt',
        'Expected mime type' => 'text/plain',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   *
   * No need for verifying the download plugin's reuse: that should be tested by
   * the download plugin's test.
   *
   * @dataProvider providerSuccessfulReuseStatic
   */
  public static function providerSuccessfulReuseStatic(): array {
    return [
      'Reusing a local file' => [
        'local_source_path' => static::getDrupalRoot() . '/core/tests/fixtures/files/image-test.jpg',
        'local_destination_path' => 'public://file1.jpg',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @dataProvider providerSuccessfulReuseStatic
   */
  public function testSuccessfulReuse($source_path, $destination_path): void {
    $file_reuse = $this->doTransform($source_path, $destination_path);
    clearstatcache(TRUE, $destination_path);
    $timestamp = time() - 10;
    touch($file_reuse, $timestamp);

    // We need to make sure the modified timestamp on the file is sooner than
    // the attempted migration.
    $configuration = ['file_exists' => 'use existing'];
    $this->doTransform($source_path, $destination_path, $configuration);
    clearstatcache(TRUE, $destination_path);
    $this->assertEquals(
      $timestamp,
      (new \SplFileInfo($destination_path))->getMTime(),
    );

    $this->doTransform($source_path, $destination_path);
    clearstatcache(TRUE, $destination_path);
    $this->assertGreaterThan(
      $timestamp,
      (new \SplFileInfo($destination_path))->getMTime(),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function doTransform($source_path, $destination_path, $configuration = [], bool $expect_download = FALSE): string {
    $executable = $this->prophesize(MigrateExecutableInterface::class)->reveal();
    $row = $this->prophesize(Row::class)->reveal();
    $download_plugin = $this->createMock(MigrateProcessInterface::class);
    if ($expect_download) {
      $download_plugin
        ->expects($this->once())
        ->method('transform');
    }
    else {
      $download_plugin
        ->method('transform')
        ->will($this->returnCallback(function ($value) {
          throw new MigrateException(sprintf(
            'Client error: ... (%s)',
            $value[0]
          ));
        }));
    }

    $plugin = new FileCopyOrGenerate(
      $configuration,
      'file_copy_or_generate',
      [],
      $this->container->get('stream_wrapper_manager'),
      $this->container->get('file_system'),
      $download_plugin
    );

    return $plugin->transform(
      [
        $source_path,
        $destination_path,
      ],
      $executable,
      $row,
      'destination_property'
    );
  }

}
