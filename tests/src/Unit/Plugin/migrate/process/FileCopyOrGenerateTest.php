<?php

declare(strict_types=1);

namespace Drupal\Tests\migrate_devel_file_copy\Unit\Plugin\migrate\process;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\migrate\Plugin\MigrateProcessInterface;
use Drupal\migrate_devel_file_copy\Plugin\migrate\process\FileCopyOrGenerate;

/**
 * Tests the file copy process plugin.
 *
 * @group migrate_devel_file_copy
 *
 * @coversDefaultClass \Drupal\migrate_devel_file_copy\Plugin\migrate\process\FileCopyOrGenerate
 */
class FileCopyOrGenerateTest extends FileCopyTestBaseShim {

  /**
   * Evaluates the assertion of ::assertPlugin.
   */
  protected function doAssertPlugin(array $configuration, $expected): void {
    $stream_wrapper_manager = $this->prophesize(StreamWrapperManagerInterface::class)->reveal();
    $file_system = $this->prophesize(FileSystemInterface::class)->reveal();
    $download_plugin = $this->prophesize(MigrateProcessInterface::class)->reveal();
    $this->plugin = new TestFileCopyOrGenerate(
      $configuration,
      'test',
      [],
      $stream_wrapper_manager,
      $file_system,
      $download_plugin
    );
    $plugin_config = $this->plugin->getConfiguration();
    $this->assertArrayHasKey('file_exists', $plugin_config);
    $this->assertSame($expected, $plugin_config['file_exists']);
  }

}

/**
 * Class for testing FileCopyOrGenerate.
 */
class TestFileCopyOrGenerate extends FileCopyOrGenerate {

  /**
   * Gets this plugin's configuration.
   *
   * @return array
   *   An array of this plugin's configuration.
   */
  public function getConfiguration() {
    return $this->configuration;
  }

}
