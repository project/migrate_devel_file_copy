<?php

declare(strict_types=1);

/**
 * @file
 * Shim for Drupal core's file copy unit test.
 */

namespace Drupal\Tests\migrate_devel_file_copy\Unit\Plugin\migrate\process;

use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Core\File\FileExists;
use Drupal\Tests\migrate\Unit\process\FileCopyTest;

$is_legacy = DeprecationHelper::backwardsCompatibleCall(
  \Drupal::VERSION,
  '10.3',
  fn () => FALSE,
  fn () => TRUE,
);
if ($is_legacy) {
  /**
   * Shim for Drupal core 10.2-.
   */
  abstract class FileCopyTestBaseShim extends FileCopyTest {

    /**
     * {@inheritdoc}
     */
    protected function assertPlugin(array $configuration, int $expected): void {
      $this->doAssertPlugin($configuration, $expected);
    }

  }
}
else {
  /**
   * Shim for Drupal core 10.3+.
   */
  abstract class FileCopyTestBaseShim extends FileCopyTest {

    /**
     * {@inheritdoc}
     */
    protected function assertPlugin(array $configuration, FileExists $expected): void {
      $this->doAssertPlugin($configuration, $expected);
    }

  }
}
