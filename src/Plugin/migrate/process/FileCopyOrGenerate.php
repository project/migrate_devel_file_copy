<?php

declare(strict_types=1);

namespace Drupal\migrate_devel_file_copy\Plugin\migrate\process;

use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Component\Utility\Random;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\migrate\process\FileCopy;
use Drupal\migrate\Row;

/**
 * Copies or moves a local file from one place into another.
 *
 * If neither the source file, nor the destination file exists, then
 * this plugin generates a file.
 *
 * Apart from the above, it works like the parent plugin.
 *
 * @todo Add configuration option for logging a message when a file is missing.
 *
 * @see \Drupal\migrate\Plugin\migrate\process\FileCopy
 *
 * @MigrateProcessPlugin(
 *   id = "file_copy_or_generate"
 * )
 */
class FileCopyOrGenerate extends FileCopy {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property): ?string {
    try {
      return parent::transform($value, $migrate_executable, $row, $destination_property);
    }
    catch (MigrateException $parent_exception) {
      [$source, $destination] = $value;
      // If the destination file exists, return its path.
      if (file_exists($destination)) {
        // @todo Log.
        return $destination;
      }

      if (
        (
          !file_exists($source) &&
          preg_match('/^File .+ does not exist$/', $parent_exception->getMessage())
        ) ||
        !$this->isLocalUri($source)
      ) {
        // Check if a writable directory exists, and if not try to create it.
        $dir = $this->getDirectory($destination);
        // If the directory exists and is writable, avoid
        // \Drupal\Core\File\FileSystemInterface::prepareDirectory() call and
        // write the file to destination.
        if (
          (!is_dir($dir) || !is_writable($dir)) &&
          !$this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)
        ) {
          throw new MigrateException("Could not create or write to directory '$dir'");
        }

        // Put a generated file in the destination directory.
        $extension = pathinfo($source, PATHINFO_EXTENSION);
        $extension_lowercased = strtolower($extension);
        // We have to lowercase file extension in order being able to generate
        // destination file for 'iMaGe.JPEG'.
        if ($extension !== $extension_lowercased) {
          $original_extension = $extension;
          $preg_safe = preg_quote($original_extension, '/');
          $destination = preg_replace("/\.{$preg_safe}$/", ".$extension_lowercased", $destination);
        }

        switch ($extension_lowercased) {
          case 'jpg':
          case 'jpeg':
          case 'gif':
          case 'png':
          case 'bmp':
            $final_destination = $this->generateImage($destination, $original_extension ?? NULL);
            break;

          case 'txt':
          default:
            $final_destination = $this->generateText($destination);
        }

        if (empty($final_destination) || !file_exists($final_destination)) {
          throw new MigrateSkipRowException(sprintf(
            "Could not generate destination file '%s' for the missing source file '%s'",
             $destination,
             $source
          ));
        }

        return $final_destination;
      }
    }

    throw $parent_exception;
  }

  /**
   * Generates an image at the given destination.
   *
   * @param string $destination
   *   The destination where the image should be generated.
   * @param string|null $original_extension
   *   The original (uppercase) extension of the file.
   *
   * @return string|null
   *   The final destination where the image was generated. If an error happened
   *   during image generation, this function will generate a text file.
   */
  protected function generateImage(string $destination, ?string $original_extension): ?string {
    try {
      $generated = (new Random())->image($destination, '200x200', '600x600');
      if ($original_extension) {
        $actual_extension = pathinfo($generated, PATHINFO_EXTENSION);
        $preg_safe = preg_quote($actual_extension, '/');
        $final_destination = preg_replace("/\.{$preg_safe}$/", ".$original_extension", $generated);
        $fileExistReplace = DeprecationHelper::backwardsCompatibleCall(
          \Drupal::VERSION,
          '10.3',
          // @phpstan-ignore-next-line
          fn () => FileExists::Replace,
          // @phpstan-ignore-next-line
          fn () => FileSystemInterface::EXISTS_REPLACE,
        );
        return $this->fileSystem->move($generated, $final_destination, $fileExistReplace);
      }
      return $generated;
    }
    catch (\Throwable) {
      return $this->generateText($destination);
    }
  }

  /**
   * Generates a text file at the given destination.
   *
   * @param string $destination
   *   The destination where the text file should be generated.
   *
   * @return string|null
   *   The final destination where the text file was generated, or NULL on
   *   failure.
   */
  protected function generateText(string $destination): ?string {
    $result = file_put_contents($destination, str_repeat('*', 4));
    if ($result !== FALSE) {
      return $destination;
    }
    return NULL;
  }

}
